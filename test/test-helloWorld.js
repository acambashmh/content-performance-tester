const assert = require('assert');
const HelloWorld = require('../js/helloWorld');
const enzyme = require('enzyme');
const React = require('react');

describe('Array', function () {
    describe('#indexOf()', function () {
        it('should return -1 when the value is not present', function () {
            var wrapper = enzyme.shallow(<HelloWorld sasa="idemooo" />);
            assert.equal(-1, [1, 2, 3].indexOf(5));
            //assert.equal(-1, [1,2,3].indexOf(0));
        });
    });
});