
require("babel-register");
require('babel-polyfill');

var http = require('http');
var express = require('express');
var app = express();
var path = require('path');
var getData = require('./server/contentAnalyser/contentAnalyser');
var bodyParser = require('body-parser');
var fs = require('fs');

if(fs.existsSync(path.join(__dirname, '/.env'))){
    require('dotenv').load();
}

if (process.env.NODE_ENV !== 'production' && process.env.NODE_ENV !== 'test') {
    // Step 1: Create & configure a webpack compiler
    var webpack = require('webpack');
    var webpackConfig = require(process.env.WEBPACK_CONFIG ? process.env.WEBPACK_CONFIG : './webpack.config');
    var compiler = webpack(webpackConfig);

    // Step 2: Attach the dev middleware to the compiler & the server
    app.use(require("webpack-dev-middleware")(compiler, {
        noInfo: false, publicPath: webpackConfig.output.publicPath
    }));

    // Step 3: Attach the hot middleware to the compiler & the server
    app.use(require("webpack-hot-middleware")(compiler, {
        log: console.log, path: '/__webpack_hmr', heartbeat: 10 * 1000
    }));
}


app.use('/', express.static(__dirname + '/public'));
app.use(bodyParser.json());


app.post('/getData', function (req, res) {
    getData(req.body.url, req.body.username, req.body.password)
        .then(function (data) {
            res.send(data);
        }).catch(function (err) {
            res.status(500).send('Server error');
        })
});


if (require.main === module) {
    var server = http.createServer(app);
    server.listen(process.env.PORT || 1616, function () {
        console.log("Listening on %j", server.address());
    });
}


module.exports = app;