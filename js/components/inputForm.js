import React from 'react'
import findDOMNode from 'react-dom'

var Inputform = React.createClass({
    getFormData: function () {
        return {
            url: this.refs.url.value,
            username: this.refs.username.value,
            password: this.refs.password.value
        }
    },
    handleFormSubmit: function (e) {
        e.preventDefault();
        var data = this.getFormData();
        this.props.handleFormSubmit(data);
    },
    render: function () {
        return (
            <div className="col-lg-offset-2">
            <form className="form-inline">
                <input type="text" className="form-control" ref="url" placeholder="Resource URL"/>
                <input type="text" className="form-control" ref="username" placeholder="User name"/>
                <input type="text" className="form-control" ref="password" placeholder="Password"/>
                <input type="submit" className="btn btn-primary" value="Get performance data"
                       onClick={ this.handleFormSubmit }/>
            </form>
            </div>
        );
    }
});

export default Inputform