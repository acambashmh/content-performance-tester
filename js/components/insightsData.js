import React from 'React';
import {connect} from 'react-redux';




const page = React.createClass({

    render: function(){

        if(this.props.insightsData){
            return <div>
                <h3>Overall score is: <strong>{this.props.insightsData.o}</strong> </h3>

                <dl>
                    <dt>Time to render</dt>
                    <dd>{this.props.insightsData.lt} milisec</dd>
                    <dt>Size</dt>
                    <dd>{this.props.insightsData.w /1024/1024 } MB </dd>
                    <dt>Total number of requests</dt>
                    <dd>{this.props.insightsData.r} </dd>
                    <dt>File types</dt>
                    <dd>
                        <ul>
                            {Object.keys(this.props.insightsData.stats).map((key) => {
                                const obj = this.props.insightsData.stats[key];
                                return <li>
                                        <span className="label label-info">{key}</span>
                                        Number of files: {obj.r} , Sum size {obj.w /1024}
                                </li>
                            })}
                        </ul>
                    </dd>
                    <dt>Analysis types</dt>
                    <dd>
                        <ul>
                            {Object.keys(this.props.insightsData.g).map((key) => {
                                const obj = this.props.insightsData.g[key];
                                return <li>
                                    <span className="label label-info">{key.substring(1, key.length)}</span>
                                    Score: {obj.score}
                                </li>
                            })}
                        </ul>
                    </dd>
                </dl>
            </div>
        }
        if(this.props.dataLoading){
            return <div className="text-center">
                <i className="fa fa-5x fa-spinner fa-spin"></i>
            </div>
        }
        return <div className="text-center">
            <strong>No data yet</strong>
        </div>
    }

});


export default page;