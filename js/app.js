import '../node_modules/bootstrap/dist/css/bootstrap.css';
import '../node_modules/font-awesome/scss/font-awesome.scss';
import '../css/app.scss';

import React from 'react';
import {render} from 'react-dom';
import Layout from './pages/layout';
import thunk from 'redux-thunk';
import { createStore, applyMiddleware, compose, combineReducers } from 'redux';
import {Provider} from 'react-redux';

//
import appReducer from './reducers/mainReducer';
//
const reducer = combineReducers(Object.assign({}, {app:appReducer}));
let store = compose(applyMiddleware(thunk)(createStore))(reducer);




render(
    <Provider store={store}>
        <Layout />
    </Provider>

    , document.getElementById('app'));
