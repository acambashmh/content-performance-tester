import React from 'React';
import {connect} from 'react-redux';
import MainForm from '../components/inputForm';
import InsightsData from '../components/insightsData';
import * as mainActions from '../actions/mainActions';

const mapStateToProps = (state) => {
    return {
        insightsData : state.app.insightsData,
        dataLoading : state.app.dataLoading,
        error: state.app.error
    }
};

const mapDispatch = (dispatch) => {
    return {
        getInsightsData : (formData) => {
            dispatch(mainActions.getInsightsData(formData))
        }
    }
};

const renderDataOrError = (props) =>{
    if(props.error){
        return <div>
            <h3 className="text-center">Server error: issue with getting insights data</h3>
        </div>
    }
    return  <InsightsData {...props} />
};

const page = React.createClass({

    render: function(){
        return <div>
            <MainForm handleFormSubmit={this.props.getInsightsData} />
            <hr />
            <div>
                <h2 className="text-center"  >Content insights</h2>
                {renderDataOrError(this.props)}
            </div>
        </div>
    }

});


export default connect(mapStateToProps, mapDispatch)(page)