import Main from './main';
import React from 'React';
import {connect} from 'react-redux';




const mapStateToProps = (state) => {
    return {
        error: state.app.error
    }
};

const layout = ({}) => {

	return <div>
	
		<div className="container"> 
			<Main />
		</div>
	</div>
};

export default connect(mapStateToProps)(layout)