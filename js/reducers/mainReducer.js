import {GET_INSIGHTS_DATA, GET_INSIGHTS_DATA_ERROR,
    GET_INSIGHTS_DATA_LOADING, GET_INSIGHTS_DATA_SUCCESS} from '../constants/mainConstants';


export default (state = {}, action) => {
    switch (action.type){
        case GET_INSIGHTS_DATA_LOADING:
            return {dataLoading: true};
        case GET_INSIGHTS_DATA_ERROR:
            return {error:action.err};
        case GET_INSIGHTS_DATA_SUCCESS:
            return {insightsData:action.insightsData};
        default:
            return state;
    }
}