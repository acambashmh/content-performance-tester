import {GET_INSIGHTS_DATA, GET_INSIGHTS_DATA_ERROR,
    GET_INSIGHTS_DATA_LOADING, GET_INSIGHTS_DATA_SUCCESS} from '../constants/mainConstants';
import axios from  'axios'

export const getInsightsData = (formData) => {
    return (dispatch) => {
        dispatch(handleInsightsDataLoading());

        axios.post('/getData', formData)
            .then((response) => {
                if(response.status === 200){
                    return dispatch(handleInsightsDataSuccess(response.data));
                }
                dispatch(handleInsightsDataError('Server error'));
            })
        .catch((err) => {
            console.log(err);
            dispatch(handleInsightsDataError(err));
        })
    }
};

export const handleInsightsDataSuccess = (insightsData) => {
    return {
        type:GET_INSIGHTS_DATA_SUCCESS,
        insightsData
    }
};

export const handleInsightsDataLoading = () => {
    return {
        type:GET_INSIGHTS_DATA_LOADING
    }
};

export const handleInsightsDataError = (err) => {
    return {
        type:GET_INSIGHTS_DATA_ERROR,
        err
    }
};