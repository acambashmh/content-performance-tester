import React from 'react'
import findDOMNode from 'react-dom'

var Inputform = React.createClass({
  getFormData: function () {
    return {
      url: this.refs.url.value,
      username: this.refs.username.value,
      password: this.refs.password.value
    }
  },
  handleFormSubmit: function (e) {
    e.preventDefault();
    var data = this.getFormData();
    this.props.handleFormSubmit(data);
  },
  render: function () {
    return (
      <form className="inputForm" >
      <input type="text" ref= "url" placeholder= "Resource URL" />
      <input type="text" ref= "username" placeholder= "User name" />
      <input type="text" ref= "password" placeholder= "Password" />
      <input type="submit" value= "Get performance data" onClick= { this.handleFormSubmit } />
      </form>
    );
  }
});

export default Inputform