import React from 'react'
import findDOMNode from 'react-dom'
import InputForm from './inputForm'

var Results = React.createClass({
  getInitialState: function(){
    return {
      data:{
        o: 0, //overall score
        w: 0, //total page size
        u: '', //URL
        r: 0, //total number of requests
        lt: 0 // page load time
        
      }
    }
  },
getFormData: function(){
    return {
      url: this.refs.url.value,
      username: this.refs.username.value,
      password: this.refs.password.value
    }
  },
  postJSON: function(url, obj, cb) {
    var req = new XMLHttpRequest();
    req.onload = function () {
      cb(JSON.parse(req.response))
    }
    req.open('POST', url)
    req.setRequestHeader('Content-Type', 'application/json;charset=UTF-8')
    req.setRequestHeader('authorization', localStorage.token)
    req.send(JSON.stringify(obj))
  },
  
    getData: function(data) {
      var getDataUrl = 'http://localhost:1616/getData';
      var that = this;
  
      this.postJSON(getDataUrl, data, function(data){
        that.setState({data: data});
        that.props.addToHistory(data);
      })
  },
    render: function() {
    return (
      <div>
        <InputForm handleFormSubmit={this.getData}></InputForm>
        <h1>Overall score: {this.state.data.o}</h1>
        <hr/>
        <p>URL: {decodeURIComponent(this.state.data.u)}</p>
        <ul>
            <li>Total page size: {this.state.data.w}</li>          
            <li>Total number of requests: {this.state.data.r}</li>
            <li>Page load time: {this.state.data.lt}</li>
        </ul>
      </div>
    );
  }
});

export default Results