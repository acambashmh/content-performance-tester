var YSLOW = require('yslow').YSLOW;
var doc = require('jsdom').jsdom();
var request = require('request');
var fetch = require('node-fetch');

function analyzeHarData(data){
        var res = YSLOW.harImporter.run(doc, data, 'ydefault');
        var content = YSLOW.util.getResults(res.context, 'all');
        return content;
}


module.exports = analyzeHarData;

