var request = require('request');
var setCookie = require('set-cookie-parser');
var Promise = require('bluebird');

module.exports = function (svr, uname, pwd, state, district, school) {
	return new Promise(function (resolve, reject) {
		var result = {};
		var response = {}
		response.headers = { 'set-cookie': "" };

		var form = {
			"organizationId": '',
			"REQUEST_TYPE": "",
			"openToken": "",
			"country": "US",
			"internationalUserLoginEnabled": true,
			"state": state || "CA",
			"district": district || 88200009,
			"school": school|| 88200010,
			"userName": uname,
			"password": pwd,
			"login2": "Log In",
			"districtNameEval": "",
			"selectedSchool": "",
			"loginpage": true
		};

		var opts = {
			"url": svr + '/ePC/login.do',
			"form": form,
			"headers": { "set-cookie": response.headers['set-cookie'] }
		};

		request.post(opts, function (error, response, body) {

			if (response.statusCode === 302) {
				populateCookie(response, result);
				var allCookies = response.headers['set-cookie'];
				var hubopts = {
					"url": response.headers['location'],
					"headers": { "cookie": response.headers['set-cookie'] }
				}

				request.get(hubopts, function (error, resp, body) {
					if (error) { console.log(error) }
					allCookies += '; ' + resp.headers['set-cookie']
					resolve(objToArray(result));
				})
			} else {
				console.log("NO 302 ERROR");
			}
		});
	});
}

function populateCookie(response, result) {
	var tempCookie = setCookie.parse(response);
	if (tempCookie && tempCookie.length > 0) {
		tempCookie.forEach(function (cookie) {
			result[cookie.name] = cookie;
		})
	}
	return result;
}

function objToArray(obj) {
	if (obj) {
		return Object.keys(obj).map(function (key) {
			return obj[key];
		});
	} else {
		return [];
	}
}