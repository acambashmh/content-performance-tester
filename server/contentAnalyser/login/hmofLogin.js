var request = require('request');
var $ = require('cheerio');
var setCookie = require('set-cookie-parser');
var Promise = require('bluebird');

module.exports = function(svr, uname, pwd) {
	return new Promise(function (resolve, reject) {
		request.get(svr, function(error, response, body) {
		
			var result = {};
		var opts = {   "url"      : svr + '/index.jsp',
					"form"     : { "username": uname, "password": pwd },
					"headers"  : { "set-cookie" : response.headers['set-cookie']}
					};
	
		request.post(opts, 
			function(error, response, body){
				if(response.statusCode === 302) {
					var hubopts = {
						"url"      : svr + response.headers['location'],
						"headers"  : { "cookie" : response.headers['set-cookie']}
						}
						populateCookie(response,result);
						
						request.get(hubopts, function (error, resp, body) {
									if(error) { 
										
										console.log(error)
										return reject('login error');
										}
									
									populateCookie(resp,result);
										hubopts.headers.cookie.push(resp.headers['set-cookie'][0]);
										//hubopts.headers.cookie[0] = 'route=1;';
										var cookie = hubopts.headers.cookie
										var cookiereq = "";
										for(var i=0;i< cookie.length;i++){
											var arr = cookie[i].split(' ')
											var c = "";
											cookiereq += arr[0] + '; ';
										}
								var tstopts = {
								"url"      : svr + "/HRWHoap/ltiLaunch/workbench/login",
								"headers"  : { "cookie" : cookiereq,
											"referer" : svr + "/hrw/my_hrw_student_hub.jsp"
											}
								}
			
								request.get(tstopts, function (error, resp, body) {
									if(error) { 
										console.log(error);
										return reject('login error');
										}
									//Parse the body and launch	
									var form = {} 
									var parsedHTML = $.load(body);
									parsedHTML('input').map(function(i, link) {
										form[$(link).attr('name')] = $(link).attr('value')
									})
									var opts = {   "url"      : svr + '/dashboard/lti',
												"form"     : form,
												"headers"  : { "set-cookie" : cookiereq}
												};
									populateCookie(resp,result);
									request.post(opts, function(error, response, body){
											if(error){
												return reject('login error');
											}
											if(response.statusCode === 302) {	
												populateCookie(response,result);
												cookiereq += ' ' + response.headers['set-cookie'].toString().split(' ')[0]
												resolve(objToArray(result));
												
											}else{
												return reject('login error');
											}
									});
								});
						});
					}
					else{
						return reject('login error');
					}	
			});
		});
	});
}

function populateCookie(response, result){
	var tempCookie = setCookie.parse(response);
	if(tempCookie && tempCookie.length > 0){
		tempCookie.forEach(function (cookie) {
			result[cookie.name] = cookie;
		})
	}
	return result;
}

function objToArray(obj){
	if(obj){
		return Object.keys(obj).map(function (key) {
			return obj[key];
		});
	}else{
		return [];
	}
}