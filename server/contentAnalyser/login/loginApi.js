
var request = require('request');
var q = require('q-bluebird');
var Promise = require('bluebird');
var url = require('url');
var settings = require('../settings.json');
var hmofLogin = require('./hmofLogin');
var tcLogin = require('./tcLogin');


function extractPlatform(resourceUrl) {
	console.log('login api resource url: ', resourceUrl);
	if (resourceUrl.indexOf('thinkcentral') > -1) {
		return 'tc';
	} else {
		return 'hmof'
	}
}

function extractEnvironment(resourceUrl) {
	if (resourceUrl.indexOf('review-cert') > -1) {
		return 'certrv'
	} else if (resourceUrl.indexOf('w-cert') > -1) {
		return 'cert'
	} else if (resourceUrl.indexOf('dubint') > -1 || resourceUrl.indexOf('my-test') > -1) {
		return 'int'
	} else if (resourceUrl.indexOf('k6') > -1 || resourceUrl.indexOf('my.hrw.com') > -1) {
		return 'prod'
	}
	throw new Error('cant extract environment from url: ' + resourceUrl);
}


module.exports = function (resourceUrl, username, password) {

    if(!username || !password){
        console.log('no Auth requested');
        return new Promise((resolve) => {
            resolve(null);
        });
    }


	return new Promise(function (resolve, reject) {
		try {
			resolve({
				platform : extractPlatform(resourceUrl),
				environment : extractEnvironment(resourceUrl)
			});
			          

		} catch (error) {
			return reject(error);
		}
	}).then(function (data) {
		var contentServerUrl = '';
		if(data.platform === 'tc'){
			contentServerUrl = settings[data.environment].contentServerTC;
			return tcLogin(contentServerUrl, username, password);
		}else{
			contentServerUrl = settings[data.environment].contentServerHMOF;
			return hmofLogin(contentServerUrl, username, password);
		}
	});
};

		
		
		
