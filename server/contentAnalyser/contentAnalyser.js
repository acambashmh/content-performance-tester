var phantomhar = require('./phantomhar/phantomhar.js');
var yslow = require('./phantomhar/yslow');
var loginApi = require('./login/loginApi');

function getData(resourceUrl, username, password, delay) {
    return loginApi(resourceUrl, username, password)
        .then(function (cookies) {
            var options = {
                url: resourceUrl,
                cookies: cookies,
                delay: delay || 5
            };
            
             return phantomhar.har(options)
        })
        .then(function (data) {
            return yslow(data);
        })
}

module.exports = getData;