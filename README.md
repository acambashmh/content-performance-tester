#content-insight tool
is a tool that's meant for analysing the content and providing insight if some of the content attributes 
are not properly implemented

##Technologies
The tech stack for this tool is:
###Client
* React
* Redux
* React-router
...
###Server
* Yslow
* Node
* phantomjs
* express
* request

###Build tools
- webpack
- rimraf
- npm


##Installing
- this requires node version 4+
- clone the repo
- npm install -g webpack
- npm install rimraf -g 
- npm start to start developing
- npm build to build the repo
- set up phantomjs http://phantomjs.org/build.html in order to be able to use module for analysing page
- run `node server` with NODE_ENV environment variable set to production(on win `set NODE_ENV=production&&node server`)
- go to http://localhost:1616

##Using it 
- go to http://localhost:1616
- enter content url, username and password for that platform and environment and submit form
- shortly if the server analysis is successful the data page insights will be shown
